package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.io.File;

import javax.swing.JOptionPane;

import modele.Donnees;
import modele.Evenement;
import modele.Frise;
import vue.LectureEcriture;
import vue.PanelAffichage;
import vue.PanelAjoutEvenement;
import vue.PanelChargementFrise;
import vue.PanelCreationFrise;
import vue.PanelFrise;

public class Controleur extends MouseAdapter implements ActionListener {
	
	private Frise chFrise ;
	private PanelAffichage panelAffichage ;
	private PanelCreationFrise panelCreation ;
	private PanelAjoutEvenement panelAjout ;
	private PanelChargementFrise panelChargement ;
	PanelFrise panelFrise;
	
	/**
	 * Classe permettant la synchronisation entre les �l�ments du modele et les �l�ments de la vue
	 * @author Thomas Carpentier et Carmen Pr�vot
	 * @param parPanelFrise
	 * @param parFrise
	 * @param parPanelAffichage
	 * @param parPanelCreation
	 * @param parPanelAjoutEvenement
	 * @param parPanelChargement
	 */
	public Controleur(PanelFrise parPanelFrise, Frise parFrise, PanelAffichage parPanelAffichage, 
			PanelCreationFrise parPanelCreation, PanelAjoutEvenement parPanelAjoutEvenement, 
			PanelChargementFrise parPanelChargement) {
		panelFrise = parPanelFrise;
		chFrise = parFrise ;
		panelAffichage = parPanelAffichage ;
		panelCreation = parPanelCreation ;
		panelAjout = parPanelAjoutEvenement ;
		panelChargement = parPanelChargement ;
		
		panelAffichage.setFrise(chFrise) ;
		
		panelCreation.enregistreEcouteur(this);
		panelAjout.enregistreEcouteur(this) ;
		panelChargement.enregistreEcouteur(this) ;
		panelFrise.panelAffichage.panelTableEvts.enregistreEcouteur(this);
		
	}
	
	public Controleur(PanelFrise parPanelFrise, PanelCreationFrise parPanelCreationFrise, PanelChargementFrise parPanelChargement) {
		panelCreation = parPanelCreationFrise;
		panelChargement = parPanelChargement;
		panelFrise = parPanelFrise;
		
		panelCreation.enregistreEcouteur(this);
		panelChargement.enregistreEcouteur(this);
	}


	@Override
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals(Donnees.INTITULE_BOUTON_CREATION)) {
			Frise frise = panelCreation.getFrise() ;
			
			if (frise.getTitre().equals(""))
				JOptionPane.showMessageDialog(panelAffichage, "Impossible de cr�er une frise." , "ERREUR", JOptionPane.ERROR_MESSAGE);
		
			else {
				File fichierFrise = new File("frises"+File.separator+"frise.ser") ;
				LectureEcriture.ecriture(fichierFrise, frise) ;
			
				panelCreation.reset () ;
				
				// panelAffichage.setFrise(frise) ;
				panelFrise.amorcage(frise);
				
			}
		
		}
		
		if (evt.getActionCommand().equals(Donnees.INTITULE_BOUTON_AJOUT)) {
			Evenement evenement = panelAjout.getEvenement() ;
			if (evenement.getTitre().equals(""))
				JOptionPane.showMessageDialog(panelAffichage, "Veuillez entrer  un titre." , "ERREUR", JOptionPane.ERROR_MESSAGE);
			
			else {
				chFrise.ajout(evenement);
				File fichierFrise = new File("frises"+File.separator+"frise.ser") ;
				LectureEcriture.ecriture(fichierFrise, chFrise) ;
			
				System.out.println(chFrise.toString());
				panelAjout.reset () ;
			
				panelAffichage.ajoutEvenement(evenement) ;
			}
		}
		
		if (evt.getActionCommand().equals("passage")) {
			panelFrise.panelAffichage.afficheEvenement(panelFrise.panelAffichage.panelTableEvts.getEvtSelectionne());
			System.out.println(panelFrise.panelAffichage.panelTableEvts.getEvtSelectionne().toString());
		}
		
		
	}//gestion des interactions

}
