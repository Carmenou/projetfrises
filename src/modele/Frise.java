package modele;

import java.io.Serializable;
import java.util.Collection;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Cette classe
 * @author Carmen Prevot & Thomas Carpentier
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Frise implements Serializable {
	private Date chDateDebut;
	private Date chDateFin;
	private TreeMap<Integer, TreeSet<Evenement>> treeMapEnsembleEvenements;
	private String chTitre;
	private int chPeriode;
	private static int nbEvts ;
	
	public Frise(String parTitre, Date parDateDebut, Date parDateFin) {
		setDateDebut(parDateDebut);
		setDateFin(parDateFin);
		treeMapEnsembleEvenements = new TreeMap<Integer, TreeSet<Evenement>>();
		setTitre(parTitre);
		chPeriode = 1 ;
		nbEvts = 0;
	} // constructeur
	
	public Frise(String parTitre, Date parDateDebut, Date parDateFin, int parPeriode) {
		setDateDebut(parDateDebut);
		setDateFin(parDateFin);
		treeMapEnsembleEvenements = new TreeMap<Integer, TreeSet<Evenement>>();
		setTitre(parTitre);
		chPeriode = parPeriode ;
		nbEvts = 0;
	} // constructeur
	
	public Frise(String parTitre) {
		setDateDebut(new Date());
		setDateFin(new Date());
		treeMapEnsembleEvenements = new TreeMap<Integer, TreeSet<Evenement>>();
		setTitre(parTitre);
		chPeriode =1 ;
		nbEvts = 0;
	}

	/**
	 * M�thode qui r�cup�re les �v�nements d'une ann�e de la frise
	 * @param parAnnee est l'ann�e donn�e en param�tre de la m�thode
	 * @return un TreeSet d'�v�nements ayant pour ann�e celle donn�e en param�tre
	 */
	public TreeSet<Evenement> getEvenementsAnnee(int parAnnee) {
		return treeMapEnsembleEvenements.get(parAnnee); 
	}
	
	public TreeSet<Evenement> getEvenement(int parAnnee, int parPoids) {
		// methode a completer
		return null;
	}
	
	public Collection<TreeSet<Evenement>> getEvenements() {
		return treeMapEnsembleEvenements.values() ;
	}
	
	public TreeSet<Evenement> getEvenements2() {
		TreeSet <Evenement> ensEvts = new TreeSet<Evenement> () ;
		for (TreeSet <Evenement> sousEnsemble : treeMapEnsembleEvenements.values()) {
			ensEvts.addAll(sousEnsemble) ;
		}
		
		return ensEvts ;
	}
	
	public void ajout(Evenement parEvt) {
		
		int annee = parEvt.getDate().getAnnee() ;
		if (treeMapEnsembleEvenements.containsKey(annee)) {
			treeMapEnsembleEvenements.get(annee).add(parEvt) ;
		} //if : si on a d�j� des events cette ann�e
		
		else {
			TreeSet <Evenement> set = new TreeSet <Evenement> () ;
			set.add(parEvt) ;
			treeMapEnsembleEvenements.put(annee, set) ;
		}//Si on en avait pas, on cr�e un nouveau set avec l'ann�e comme cl� correspondante
		
		setNbEvts(getNbEvts() + 1) ;
	}
	
	public String toString() {
		String message = getTitre() + "\n" + getDateDebut().getAnnee() + " - " + getDateFin().getAnnee();
		
		for (Integer annee : treeMapEnsembleEvenements.keySet()) {
			message += "Ann�e " +annee+ " : " ;
			for (Evenement evt : treeMapEnsembleEvenements.get(annee)) {
				message+=" - " + evt.getTitre() ;
			}
		}
			
		return message;
	}

	public Date getDateDebut() {
		return chDateDebut;
	}

	public void setDateDebut(Date parDateDebut) {
		this.chDateDebut = parDateDebut;
	}

	public Date getDateFin() {
		return chDateFin;
	}

	public void setDateFin(Date parDateFin) {
		this.chDateFin = parDateFin;
	}

	public int getPeriode() {
		return chPeriode;
	}
	
	public void setPeriode(int parPeriode) {
		chPeriode = parPeriode;
	}

	public String getTitre() {
		return chTitre;
	}

	public void setTitre(String parTitre) {
		this.chTitre = parTitre;
	}

	public int getNbEvts() {
		return nbEvts;
	}

	public void setNbEvts(int parNbEvts) {
		this.nbEvts = parNbEvts;
	}
}
