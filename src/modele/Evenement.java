package modele;

import java.io.Serializable;
import java.text.ParsePosition;

/**
 * Classe d'un �v�nement compos� d'une date, d'un titre, d'une description, auquel on donne un certain poids, et illustr� par une image
 * @author Thomas Carpentier et Carmen Pr�vot
 *
 */
@SuppressWarnings("serial")
public class Evenement implements Comparable <Evenement>, Serializable {
	
	/**
	 * Champ de la date de l'�v�nement
	 * @see Date
	 * 
	 */
	private Date chDate ;
	
	/**
	 * Champ du titre de l'�v�nement
	 */
	private String chTitre ;
	
	/**
	 * Champ de la description de l'�v�nement
	 */
	private String chDescription ;

	
	/**
	 * Champ du poids de l'�v�nement
	 */
	private int chPoids ;
	
	/**
	 * Champ de l'adresse de l'image associ�e � cet �v�nement
	 */
	private String chAdresseImage ;
	

	/**
	 * Constructeur instan�iant un �v�nement � partir d'une date et d'un titre 
	 * @param parDate date de l'�v�nement � cr�er
	 * @param parTitre titre de cet �v�nement
	 */
	public Evenement (Date parDate, String parTitre, int parPoids) {
		chDate = parDate ;
		chTitre = parTitre ;
		chPoids = parPoids;
		setDescription("");
		setPoids(chPoids);
		setAdresseImage("images/default.png");
	} //constructeur Evenement()
	
	
	/**
	 * Constructeur instan�iant un �v�nement � partir d'une date, d'un titre et d'une description 
	 * @param parDate date de l'�v�nement � cr�er
	 * @param parTitre titre de cet �v�nement
	 * @param parDesc description de l'�v�nement
	 */
	public Evenement (Date parDate, String parTitre, String parDesc, int parPoids){
		chDate = parDate ;
		chTitre = parTitre ;
		chPoids = parPoids;
		setDescription(parDesc);
		setPoids(chPoids);
		setAdresseImage("images/default.png");
	} //constructeur Evenement()
	
	
	/**
	 * Constructeur instan�iant un �v�nement � partir d'une date, d'un titre, d'une description, d'un poids et d'une image donn�s 
	 * @param parDate date de l'�v�nement � cr�er
	 * @param parTitre titre de cet �v�nement
	 * @param parDesc description de l'�v�nement
	 * @param parPoids poids de l'�v�nement, c'est-�-dire son importance
	 * @param parAdrImg adresse de l'image illustrant cet �v�nement
	 */
	public Evenement (Date parDate, String parTitre, String parDesc, int parPoids, String parAdrImg){
		chDate = parDate ;
		chTitre = parTitre ;
		chPoids = parPoids;
		setDescription(parDesc);
		setPoids(parPoids);
		setAdresseImage(parAdrImg);
	} //constructeur Evenement()
	
	/**
	 * m�thode qui renvoie l'�v�nement dans un format plus lisible (format cha�ne de caract�res)
	 * @return cha�ne de caract�res qui d�crit l'�v�nement
	 */
	public String toString() {
		
		int nb_mots = 0 ;
		String sautLigne = "<br>";
		String descriptionAffichage = new String(chDescription) ;
		for (int caractere = 0; caractere<descriptionAffichage.length(); caractere++) {
			if (descriptionAffichage.charAt(caractere) == ' ' && nb_mots >= 8) {
				descriptionAffichage = descriptionAffichage.substring(0, caractere) + sautLigne + descriptionAffichage.substring(caractere+1);
				nb_mots = 0 ;
			}
			else if (descriptionAffichage.charAt(caractere) == ' ')
				nb_mots ++ ;
		}
		
		return "<html><h3>" + chDate + "</h3><h2>" + chTitre + "</h2><br>" + descriptionAffichage + sautLigne + "</html>";
	} //toString
	
	/**
	 * M�thode qui rend accessible la date de l'�v�nement
	 * @return date de l'�v�nement
	 */
	public Date getDate() {
		return chDate ;
	} //getDate()
	
	/**
	 * M�thode qui rend accessible le titre de l'�v�nement
	 * @return titre de l'�v�nement
	 */
	public String getTitre() {
		return chTitre ;
	} //getTitre()
	
	/**
	 * M�thode qui rend accessible la description de l'�v�nement
	 * @return description de l'�v�nement
	 */
	public String getDescription() {
		return chDescription ;
	}//getDecription()
	
	/**
	 * M�thode qui rend accessible le poids de l'�v�nement
	 * @return poids de l'�v�nement
	 */
	public int getPoids() {
		return chPoids ;
	}//getPoids()
	
	
	/**
	 * M�thode qui rend accessible l'adresse de l'image de l'�v�nement
	 * @return l'adresse de l'image de l'�v�nement (String)
	 */
	public String getAdresseImage() {
		return chAdresseImage ;
	}// getAdresseImage()
	
	/**
	 * M�thode qui rend modifiable la date de l'�v�nement
	 * @param parDate date � assigner � l'�v�nement
	 */
	public void setDate(Date parDate) {
		chDate = parDate ;
	}//setDate(parDate)
	
	/**
	 * M�thode qui rend modifiable le titre de l'�v�nement
	 * @param parTitre titre � assigner � l'�v�nement
	 */
	public void setTitre(String parTitre) {
		chTitre = parTitre ;
	}//setTitre(parTitre)
	
	/**
	 * M�thode qui rend modifiable la description de l'�v�nement
	 * @param parDesc description � assigner � l'�v�nement
	 */
	public void setDescription(String parDesc) {
		chDescription = parDesc ;
	}//setDecription(parDesc)
	
	/**
	 * M�thode qui rend modifiable le poids de l'�v�nement
	 * @param parPoids poids � assigner � l'�v�nement
	 */
	public void setPoids(int parPoids) {
		chPoids = parPoids ;
	}//setPoids(parPoids)
	
	/**
	 * M�thode qui rend modifiable l'adresse de l'image de l'�v�nement
	 * @param adrImage adresse de l'image � assigner � l'�v�nement
	 */
	public void setAdresseImage(String adrImage) {
		chAdresseImage = adrImage ;
	}// setAdrImage(adrImage)
	
	
	/**
	 * M�thode qui compare l'�v�nement appelant � l'�v�nement donn� en param�tre
	 * @param parEvt �v�nement que l'on compare � l'�v�nement appelant
	 * @return entier diff�rent de 0 si les �v�nements sont diff�rents, z�ro sinon
	 */
	public int compareTo(Evenement parEvt) {
		
		if (chDate.compareTo(parEvt.chDate) != 0)
			return chDate.compareTo(parEvt.chDate) ;
		
		if (chTitre.compareTo(parEvt.chTitre) != 0)
			return chTitre.compareTo(parEvt.chTitre) ;
		
		if (chDescription.compareTo(parEvt.chDescription)!= 0)
			return chDescription.compareTo(parEvt.chDescription) ;
		
		if (chPoids != parEvt.chPoids)
			return chPoids - parEvt.chPoids ;
		
		return chAdresseImage.compareTo(parEvt.chAdresseImage) ;
	} //compareTo()


	

} //class Evenement
