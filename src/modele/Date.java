package modele;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;


/**Elle stocke une date avec le jour, le mois et l'ann�e dans un format num�rique (chacun est un num�ro)
 * 
 * @author Carmen Pr�vot et Thomas Carpentier
 * @version 1
 *
 */
@SuppressWarnings("serial")
public class Date implements Comparable<Date>, Serializable {
	/**
	 * champ contenant le jour
	 */
	private int chJour;
	
	/**
	 * champ contenant le num�ro du mois
	 */
	private int chMois;
	
	/**
	 * champ contenant l'ann�e
	 */
	private int chAnnee;
	
	/**
	 * champ contenant le num�ro du jour de la semaine correspondant � la date
	 */
	private int chJourSemaine ;
	
	
	/**Constructeur par d�faut. Construit la date du jour.
	 */
	public Date() {
		GregorianCalendar aujourdhui = new GregorianCalendar () ;
		chAnnee = aujourdhui.get(Calendar.YEAR) ;
		chMois = aujourdhui.get(Calendar.MONTH) +1 ; 
		chJour = aujourdhui.get(Calendar.DAY_OF_MONTH) ;
		chJourSemaine = aujourdhui.get(Calendar.DAY_OF_WEEK) ;
		
	}//Constructeur vide -> met la date � la date du jour
	
	
	/**Constructeur qui instancie une date � partir d'un jour, d'un mois et d'une ann�e donn�s
	 * @param parJour num�ro du jour
	 * @param parMois num�ro du mois
	 * @param parAnnee num�ro du mois
	 */
	public Date(int parJour, int parMois, int parAnnee) {
		
		chJour = parJour ;
		chMois = parMois ;
		chAnnee = parAnnee ;
		
		GregorianCalendar jourConstruit = new GregorianCalendar(chAnnee, chMois-1, chJour) ;
		chJourSemaine = jourConstruit.get(Calendar.DAY_OF_WEEK) ;
		
	} //constructeur 
	
	
	
	/**M�thode qui retourne la date sous la forme d'une cha�ne de caract�res (utile pour l'affichage)
	 * @return chaine de caract�res contenant la date dans un format plus lisible
	 */
	public String toString() {
		
		return Donnees.JOURS_SEMAINE[chJourSemaine -1] + " " + chJour + " " + Donnees.NOMS_MOIS[chMois -1] + " " + chAnnee ;
	}//toString
	
	
	/**
	 * M�thode qui v�rifie si l'ann�e donn�e en param�tre est bissextile ou non.
	 * @param parAnnee ann�e � v�rifier
	 * @return bool�en indiquant si oui (True) ou non (False) l'ann�e est bissextile
	 */
	static boolean estBissextile(int parAnnee) {
		return (parAnnee%4 == 0 && parAnnee%100 != 0) || parAnnee%400 == 0 ;
	} // estBissextile
	
	
	/**
	 * M�thode qui donne le dernier jour du mois donn� � l'ann�e donn�e
	 * @param parMois mois dont on veut conna�tre le dernier jour (son num�ro)
	 * @param parAnnee ann�e � laquelle le mois appartient
	 * @return entier indiquant le num�ro du dernier jour du mois
	 */
	static int dernierJourDuMois(int parMois, int parAnnee) {
		int tabMois[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} ;
		
		if (estBissextile(parAnnee))
			tabMois[1] = 29;
		
		return tabMois[parMois -1] ;
	} //dernierJourDuMois
	
	
	/**
	 * M�thode qui compare la date appelante avec la date en param�tre
	 * @param parDate date � comparer avec la date appelante
	 * @return entier qui vaut -1 si la date appelante est ant�rieure, +1 si elle est post�rieure et 0 si les dates sont les m�mes.
	 */
	public int compareTo(Date parDate){
		if (chAnnee < parDate.chAnnee)
			return -1 ;
		if (chAnnee > parDate.chAnnee)
			return 1 ;
		
		//A ce stade les ann�es sont forc�ment ==
		
		if (chMois < parDate.chMois)
			return -1 ;
		if (chMois > parDate.chMois)
			return 1 ;
		
		//A ce stade les ann�es et les mois sont ==
		
		if (chJour < parDate.chJour)
			return -1 ;
		if (chJour > parDate.chJour)
			return 1 ;
		
		//Enfin, si on est ici c'est que ann�es, mois, et jours sont ==
		return 0 ;
	}//compareTo()

	
	/**
	 * M�thode qui donne le num�ro du jour
	 * @return num�ro du jour
	 */
	public int getJour() {
		return chJour;
	} //getJour()
	
	
	/**
	 * M�thode qui donne le num�ro du mois
	 * @return num�ro du mois
	 */
	public int getMois() {
		return chMois;
	} //getMois()
	
	
	/**
	 * M�thode qui donne l'ann�e
	 * @return num�ro de l'ann�e
	 */
	public int getAnnee() {
		return chAnnee;
	} //getMois()
	
	
	/**
	 * M�thode qui donne le num�ro du jour de la semaine
	 * @return num�ro du jour de la semaine
	 */
	public int getJourSemaine () {
		return chJourSemaine;
	} //getJourSemaine
	
	
	/**
	 * M�thode qui renvoie le num�ro de la semaine � laquelle appartient la date
	 * @return num�ro de la semaine
	 */
	public int getNumSemaine() {
		
		GregorianCalendar calendrier = new GregorianCalendar(this.getAnnee(), this.getMois() -1, this.getJour()) ;
		
		int numeroDeLaSemaine = calendrier.get(Calendar.WEEK_OF_YEAR) ; //conversion implicite
		
		return numeroDeLaSemaine ;
	}//getNumSemaine
	
	
	/**
	 * M�thode qui donne la date du lendemain de la date appelante
	 * @return date du lendemain
	 */
	public Date dateDuLendemain()  {
		
		if (chMois < 12 && chJour == Date.dernierJourDuMois(chMois, chAnnee)) {
			return new Date (1, chMois + 1, chAnnee) ;
		}
		
		if (chMois == 12 && chJour == Date.dernierJourDuMois(chMois, chAnnee)) {
			return new Date (1, 1, chAnnee +1) ;
		}
		
		return new Date (chJour +1, chMois, chAnnee) ;
	
	} //dateDuLendemain()
	
	
	/**
	 * M�thode qui donne la date de la veille de la date appelante
	 * @return date de la veille
	 */
	public Date dateDeLaVeille()  {
		
		if (chMois == 1 && chJour == 1) {
			return new Date(Date.dernierJourDuMois(12, chAnnee-1), 12, chAnnee-1) ;
		}
		
		if (chJour == 1) {
			return new Date (Date.dernierJourDuMois(chMois - 1, chAnnee), chMois -1, chAnnee) ;
		}
		
		return new Date (chJour - 1, chMois, chAnnee) ;
	
	} //dateDeLaVeille()
	
	public Date setAnnee(int parAnnee) {
		return new Date(chJour, chMois, parAnnee);
	}
	

	
}//class Date
