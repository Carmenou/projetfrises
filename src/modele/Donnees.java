package modele;

import java.awt.Font;

import javax.swing.ComboBoxModel;

public class Donnees {
	
	//Polices utilis�es :
	public final static Font font1 = new Font("Verdana",Font.BOLD,14) ;
	public final static Font font2 = new Font("Helvetica",Font.BOLD,14) ;
	public final static Font font3 = new Font("Helvetica",Font.PLAIN, 13) ;
	public final static Font font4 = new Font("Verdana",Font.PLAIN, 13) ;
	
	
	//Constantes noms des mois
	public final static String JOURS_SEMAINE [] = {"Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"} ;
	public final static String NOMS_MOIS [] = {"Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"} ;
	
	//Noms boutons ajout et cr�ation
	public static final String INTITULE_BOUTON_AJOUT = "+";
	public static final String INTITULE_BOUTON_CREATION = "Cr�er";
	
	//Items du menu
	public static final String[] ITEMS = {"Cr�ation", "Affichage", "Help", "Quitter"};
	public static final int NB_ITEMS = ITEMS.length;
	
	//Items du sous-menu
	public static final String[] SUBMENU_ITEMS = {"Charger une frise", "Cr�er une frise", "Ajouter un �v�nement"};
	public static final int NB_SUBMENU_ITEMS = SUBMENU_ITEMS.length;
	
	//Les informations sur les boutons de d�filement du panel Description (qui fait d�filer les �v�nements)
	public static final String[] INTITULE_BOUTONS_DEFILEMENT = {"<", ">"} ;
	public static final int NB_BOUTONS_DEFILEMENT = INTITULE_BOUTONS_DEFILEMENT.length;
	

}

