package vue;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleur.Controleur;

public class PanelChargementFrise extends JPanel {
	JLabel labelChargement;
	JTextField fieldChargement;
	JButton boutonChargement;
	
	public PanelChargementFrise() {
		labelChargement = new JLabel("Entrez le nom du fichier � charger :");
		fieldChargement = new JTextField(9);
		boutonChargement = new JButton("Charger");
		
		this.add(labelChargement);
		this.add(fieldChargement);
		this.add(boutonChargement);
	}
	
	public String getNomFichier() {
		return fieldChargement.getText();
	}

	public void enregistreEcouteur(Controleur controleur) {
		boutonChargement.addActionListener(controleur);
		
	}
}
