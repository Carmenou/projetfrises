package vue;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controleur.Controleur;
import modele.Date;
import modele.Donnees;
import modele.Frise;

@SuppressWarnings("serial")
/**
 * Classe du panneau permettant la cr�ation d'une nouvelle frise (formulaire de cr�ation)
 * @author Thomas Carpentier et Carmen Pr�vot
 *
 */
public class PanelCreationFrise extends JPanel {
	
	/**
	 * �tiquette g�n�rale
	 */
	//El�ments de la ligne 0
	JLabel labelCreation = new JLabel ("Cr�ation d'une frise") ;
	/**
	 *Bouton finalisant la cr�ation de la frise
	 */
	JButton boutonCreation = new JButton(Donnees.INTITULE_BOUTON_CREATION) ;
	
	//El�ments de la ligne 1
	/**
	 * Etiquette du titre
	 */
	JLabel labelTitre = new JLabel("Titre :") ;
	/**
	 * Champ d'entr�e du titre
	 */
	JTextField fieldTitre = new JTextField(8) ;
	
	//Elements de la ligne 2
	/**
	 * Etiquette du d�but de la frise
	 */
	JLabel labelDebut = new JLabel("Ann�e de d�but :");
	/**
	 * Champ de saisie de l'ann�e de d�but
	 */
	JTextField fieldDebut = new JTextField(8);
	
	
	//Element de la ligne 3
	/**
	 * Etiquette de fin de la frise
	 */
	JLabel labelFin = new JLabel("Ann�e de fin :");
	/**
	 * Champ de saisie de l'ann�e de fin de la frise
	 */
	JTextField fieldFin = new JTextField(8) ;
	
	//Elements de la ligne 4
	JLabel labelPeriode = new JLabel("P�riode :");
	
	JTextField fieldPeriode = new JTextField(8) ;

	public PanelCreationFrise () {
		setLayout (new GridBagLayout());
		GridBagConstraints contrainte = new GridBagConstraints ();
		contrainte.insets = new Insets (6,6,6,6);
		contrainte.anchor = GridBagConstraints.WEST;
		
		// Ligne 0
		contrainte.gridy = 0;
		contrainte.gridwidth = 3;
		this.add(labelCreation, contrainte);
		
		contrainte.gridx = 3;
		boutonCreation.setActionCommand(Donnees.INTITULE_BOUTON_CREATION);
		this.add(boutonCreation, contrainte);
		
		// Ligne 1
		contrainte.gridx = 0;
		contrainte.gridy++;
		
		contrainte.gridwidth = 3;
		this.add(labelTitre, contrainte);
		
		contrainte.gridx = 3;
		this.add(fieldTitre, contrainte);
		
		// Ligne 2
		contrainte.gridx = 0;
		contrainte.gridwidth = 3;
		contrainte.gridy++;
		this.add(labelDebut, contrainte);
		
		contrainte.gridx = 3;
		contrainte.gridwidth = 2;
		this.add(fieldDebut, contrainte);
		
		// Ligne 3
		contrainte.gridx = 0;
		contrainte.gridy++;
		contrainte.gridwidth = 3;
		this.add(labelFin, contrainte);
		
		contrainte.gridx = 3;
		contrainte.gridwidth = 2;
		this.add(fieldFin, contrainte);
		
		
		// Ligne 4
		/*contrainte.gridx = 0;
		contrainte.gridy++;
		contrainte.gridwidth = 3;
		this.add(labelPeriode, contrainte);
		
		contrainte.gridx = 3;
		contrainte.gridwidth = 2;
		this.add(fieldPeriode, contrainte);*/
	}
	
	public void enregistreEcouteur (Controleur parControleur) {
		boutonCreation.addActionListener(parControleur);
	}

	public Frise getFrise() {
		
		if (fieldDebut.getText().equals("") || fieldFin.getText().equals("") || fieldTitre.getText().equals(""))
			return new Frise("") ;
		
		Date dateDebut = new Date(1, 1, Integer.parseInt(fieldDebut.getText())) ;
		Date dateFin = new Date(31, 12, Integer.parseInt(fieldFin.getText())) ;
		
		if (fieldPeriode.getText() != "")
			return new Frise(fieldTitre.getText(), dateDebut, dateFin, 1);
		
		return new Frise(fieldTitre.getText(), dateDebut, dateFin);
	}

	public void reset() {
		fieldTitre.setText("");
		fieldDebut.setText("");
		fieldFin.setText("");
		fieldPeriode.setText("");
		
	}

}
