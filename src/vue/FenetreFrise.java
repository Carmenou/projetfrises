package vue;


import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;

import modele.Donnees;

@SuppressWarnings("serial")
public class FenetreFrise extends JFrame {
	
	/**
	 * Fenetre Mere du projet, c'est cette classe qui instancie et ouvre une fenetre, et y ins�re un panel fils, ici un PanelFrise
	 * @author Thomas Carpentier et Carmen Pr�vot
	 */
	public FenetreFrise () {
		
		super() ;
		
		PanelFrise contentPane = new PanelFrise() ;
		
		//Barre de menus
		JMenuBar menuBar = new JMenuBar();
		this.setJMenuBar(menuBar);
		
		//Ajout des items : 
		for (int item=0 ; item < Donnees.NB_ITEMS ; item++) {
			JMenuItem itemMenu = new JMenuItem(Donnees.ITEMS[item], Donnees.ITEMS[item].charAt(0)) ;
			itemMenu.setAccelerator(KeyStroke.getKeyStroke(Donnees.ITEMS[item].charAt(0), java.awt.Event.CTRL_MASK));
			itemMenu.addActionListener(contentPane);
			itemMenu.setActionCommand(Donnees.ITEMS[item]);
			itemMenu.setMaximumSize(new Dimension(150,60));
			menuBar.add(itemMenu);
		}
		
		setContentPane(contentPane);
		//contentPane.setBackground (Donnees.couleurFenetre);
		setDefaultCloseOperation(EXIT_ON_CLOSE) ;
		setSize (1300, 550); setVisible(true); setLocation(40, 40) ;
		
	}// constructeur FenetreFrise
	
	public static void main (String[] args){
		new FenetreFrise () ;
	} //main()
} // classe FenetreFrise



