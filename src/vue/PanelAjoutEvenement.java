package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import controleur.Controleur;
import modele.Frise;
import modele.Date;
import modele.Evenement;

/**
 * 
 * @author Thomas Carpentier et Carmen Pr�vot
 *
 */
@SuppressWarnings("serial")
public class PanelAjoutEvenement extends JPanel {
	Date date = new Date();
	
	JLabel labelTitre = new JLabel("Titre");
	
	JButton boutonAjout;
	JTextField textFieldTitre = new JTextField(9);
	
	JLabel labelIntitule = new JLabel("Ajouts d'�v�nements dans la frise");
	JLabel labelJour = new JLabel("Jour");
	JLabel labelMois = new JLabel("Mois");
	JLabel labelAnnee = new JLabel("Ann�e");
	JLabel labelPoids = new JLabel("Poids");
	
	JLabel labelDesc = new JLabel("Description");
	JTextArea textAreadDesc = new JTextArea(4,9);
	JComboBox<Integer> comboBoxJour;
	JComboBox<Integer> comboBoxMois;
	JComboBox<Integer> comboBoxAnnee;
	JComboBox<Integer> comboBoxPoids;
	
	
	public PanelAjoutEvenement(Frise parFrise) {
		setLayout (new GridBagLayout());
		GridBagConstraints contrainte = new GridBagConstraints ();
		contrainte.insets = new Insets (4,4,4,4);
		

		//bouton pour ajouter l'�v�nement � l'agenda
		this.boutonAjout = new JButton("+");
		boutonAjout.setActionCommand ("+");
		
		//Tableaux de jours et de mois pour le PanelAjoutEvenement
		final Integer tabMois[] = new Integer[12] ;
		final Integer tabJours[] = new Integer [31] ;
		
		for (int i = 1 ; i<32 ; i++) {
			tabJours[i-1] = i ;
			
			if (i<13)
				tabMois[i-1] = i ;
		}
			
		comboBoxJour = new JComboBox<Integer>(tabJours);
		comboBoxMois = new JComboBox<Integer>(tabMois);
		
		
		//Tableau des ann�es possibles
		final Integer tabAnnees[] = new Integer [parFrise.getDateFin().getAnnee() 
		                                   - parFrise.getDateDebut().getAnnee() +1];
		int debut = parFrise.getDateDebut().getAnnee();
		int i = debut ;
		while(i < parFrise.getDateFin().getAnnee()+1) {
			tabAnnees[i-debut] = i ;
			i++;
		}
		comboBoxAnnee = new JComboBox<Integer>(tabAnnees);
		
		final Integer tabPoids[] = {0,1,2,3,4};
		comboBoxPoids = new JComboBox<Integer>(tabPoids);
		
		
		// Ligne 0
		// Initule du panel et boutonAjout
		contrainte.gridwidth = 5;
		contrainte.gridx = 0;
		this.add(this.labelIntitule, contrainte);
		contrainte.gridx = 5;
		this.add(this.boutonAjout, contrainte);
		contrainte.gridwidth = 1;
		
		// Ligne 1
		// Titre
		contrainte.gridx = 0;
		contrainte.gridy = 1;
		this.add(labelTitre, contrainte);
		
		contrainte.gridx = 1;
		contrainte.gridwidth = 3;
		this.add(textFieldTitre, contrainte);
		contrainte.gridwidth = 1;
		
		
		// Ligne 2
		// Jour
		contrainte.gridx = 0;
		contrainte.gridy = 2;
		this.add(labelJour, contrainte);
		contrainte.gridx = 1;
		this.add(comboBoxJour, contrainte);
		
		// Ligne 3
		// Mois
		contrainte.gridx = 0;
		contrainte.gridy = 3;
		this.add(labelMois, contrainte);
		contrainte.gridx = 1;
		this.add(comboBoxMois, contrainte);
		
		// Ligne 4
		// Annee
		contrainte.gridx = 0;
		contrainte.gridy = 4;
		this.add(labelAnnee, contrainte);
		contrainte.gridx = 1;
		this.add(comboBoxAnnee, contrainte);
		
		// Ligne 5
		// Poids
		contrainte.gridx = 0;
		contrainte.gridy = 5;
		this.add(labelPoids, contrainte);
		contrainte.gridx = 1;
		this.add(comboBoxPoids, contrainte);
		
		// Ligne 6
		// Description
		contrainte.gridx = 0;
		contrainte.gridy = 6;
		this.add(labelDesc, contrainte);
		contrainte.gridx = 1;
		contrainte.gridwidth = 4;
		this.add(textAreadDesc, contrainte);
		contrainte.gridwidth = 1;
		
	} // constructeur
	
	
	public void reset() {
		textFieldTitre.setText(new String());
		textAreadDesc.setText(new String());
	}

	public Evenement getEvenement() {
		Date date = new Date((Integer) comboBoxJour.getSelectedItem(),(Integer) 
				comboBoxMois.getSelectedItem(),(Integer) comboBoxAnnee.getSelectedItem());
		
		return new Evenement(date, textFieldTitre.getText(), textAreadDesc.getText(), 
				(Integer) comboBoxPoids.getSelectedItem());
	}


	public void enregistreEcouteur(Controleur controleur) {
		boutonAjout.addActionListener(controleur);
	}
}
