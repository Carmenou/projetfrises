package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import modele.Evenement;

@SuppressWarnings("serial")
public class PanelEvenement extends JPanel {
	
	private JLabel informationsEvt ;
	private JLabel labelPhotoEvt ;
	private ImageIcon imageEvt ;
	
	public PanelEvenement(Evenement evt) {
		
		this.setLayout(new GridBagLayout());
		
		informationsEvt = new JLabel(evt.toString()) ;
		labelPhotoEvt = new JLabel() ;
		imageEvt = new ImageIcon(evt.getAdresseImage()) ;
		labelPhotoEvt.setIcon(imageEvt);
		
		
		GridBagConstraints contraintes = new GridBagConstraints () ;
		contraintes.insets = new Insets (2,27,2,27) ;		
		contraintes.anchor = GridBagConstraints.WEST ;
		
		contraintes.gridwidth = 2 ;
		this.add(labelPhotoEvt, contraintes) ;
		
		contraintes.gridx = 3;
		contraintes.gridwidth = 6 ;
		this.add(informationsEvt, contraintes) ;
		
	}//constructeur

}//classe PanelEvenement
