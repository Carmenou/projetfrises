package vue;

import java.util.TreeSet;

import javax.swing.table.DefaultTableModel;

import modele.Evenement;
import modele.Frise;

@SuppressWarnings("serial")
public class ModeleTable extends DefaultTableModel {
	private String intitulesColonnes [] ;
	private TreeSet<Evenement> evenementsAffiches ;
	private int anneeDebut ;
	private int anneeFin ;
	
	/**
	 * Classe permettant de servir de mod�le pour instancier une JTable
	 * @author Thomas Carpentier et Carmen Pr�vot
	 * @param parFrise
	 */
	public ModeleTable(Frise parFrise) {
		
		//On d�finit le d�but, la fin et la dur�e de la frise
		anneeDebut = parFrise.getDateDebut().getAnnee() ;
		anneeFin = parFrise.getDateFin().getAnnee() ;
		int dureeFrise = anneeFin - anneeDebut +1 ;
		intitulesColonnes = new String[dureeFrise] ;
		
		//D�finition du nombre de colonnes et de lignes
		this.setRowCount(5);
		this.setColumnCount(dureeFrise); //On part de la premi�re ann�e � la derni�re de la frise
		
		
		for (int annee = 0 ; annee < dureeFrise ; annee ++) {
			if (annee%parFrise.getPeriode() == 0)
				intitulesColonnes[annee] = "" + (anneeDebut + annee) ;
			
			else
				intitulesColonnes[annee] = " " ;
			
			evenementsAffiches = parFrise.getEvenementsAnnee(anneeDebut + annee) ;
			ajoutEvenements (evenementsAffiches, annee) ;
		}//for d�finissant les intitul�s de colonnes et ajoutant les �v�nements par ann�e
		
		this.setColumnIdentifiers(intitulesColonnes); //Les intitul�s de colonnes sont les ann�es selon la p�riode d�finie
	
	}//constructeur de la classe

	
	private void ajoutEvenements(TreeSet<Evenement> parEvenementsAffiches, int numAnnee) {
		
		if (parEvenementsAffiches != null){
			
			int numeroColonne = numAnnee ;
			int numeroLigne = 0 ;
			
			for (Evenement evt : parEvenementsAffiches) {
				numeroLigne = evt.getPoids() ;
				setValueAt(evt, numeroLigne, numeroColonne) ;
		}

		} //for parcourant tous les �v�nements � afficher
		
	}//ajoutEvenements(evenementsAafficher, anneeDeCesEvenements)
	
	public void ajoutEvenement(Evenement parEvenement) {

		int numeroColonne = parEvenement.getDate().getAnnee() - anneeDebut ;
		int numeroLigne = parEvenement.getPoids() ;
			
		while (getValueAt(numeroLigne, numeroColonne) != null && numeroLigne < 5)
				numeroLigne++ ;
		
		setValueAt(parEvenement, numeroLigne, numeroColonne) ;
	
	}//ajoutEvenements(evenementsAafficher, anneeDeCesEvenements)
	
	
	
	public boolean isCellEditable(int indiceLigne, int indiceColonne) {
		return false ;
	}//isCellEditable()
	
	
	public Class<Evenement> getColumnClass (int indiceColonne) {
		return Evenement.class ;
	}//getColumnClass()
}//Classe ModeleTable
