package vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.Controleur;
import modele.Date;
import modele.Donnees;
import modele.Evenement;
import modele.Frise;

public class PanelAffichage extends JPanel {
	
	private JPanel panelDescription ;
	public PanelTable panelTableEvts ;
	private Frise frise ;
	private JButton [] tabBoutons ;
	//private TreeSet<PanelEvenement> setPanelsEvts ;
	private TreeSet<Evenement> setEvts ;
	private JPanel panelAffichageEvts;
	private CardLayout gestionnaireCartes;
	
	/**
	 * Classe qui g�re l'affichage de la JTable affichant la frise, mais aussi du JPanel affichant la description des �v�nements
	 * @author Thomas Carpentier et Carmen Pr�vot
	 * @param parFrise
	 */
	public PanelAffichage (Frise parFrise) {
		
		setLayout (new BorderLayout (40,10)) ;
		
		frise = parFrise ;
		
		
		//mise en forme du Panel Descritpion
		panelDescription = new JPanel() ;
		//On d�finit le layout qui va organiser la fen�tre
				
		panelDescription.setLayout(new BorderLayout());
				
		//On affiche le titre de la frise tout en haut
		JLabel labelTitreFrise = new JLabel(frise.getTitre());
		panelDescription.add(labelTitreFrise, BorderLayout.NORTH) ;
				
				
		//On ajoute les boutons de d�filement
		tabBoutons = new JButton [Donnees.NB_BOUTONS_DEFILEMENT] ;

		for (int bouton = 0; bouton < Donnees.NB_BOUTONS_DEFILEMENT; bouton++) {
			tabBoutons[bouton] = new JButton(Donnees.INTITULE_BOUTONS_DEFILEMENT[bouton]);
			
			tabBoutons[bouton].setActionCommand(Donnees.INTITULE_BOUTONS_DEFILEMENT[bouton]);
			
			if (bouton < Donnees.NB_BOUTONS_DEFILEMENT/2) {
				this.add(tabBoutons[bouton], BorderLayout.WEST);
			}//Si le bouton est celui de gauche (� modifier si on a 2 boutons de chaque cot�)
			
			else {
				this.add(tabBoutons[bouton], BorderLayout.EAST);
			}//Si c'est celui de droite
		}//Instanciation des boutons
			
		
		//On cr�e le panel de d�filement
		panelAffichageEvts = new JPanel() ;
		
		//On instancie les panels 
		//setPanelsEvts = new TreeSet<PanelEvenement> () ;
		setEvts = new TreeSet<Evenement> () ;
		
		//Ensuite on utilise un CardLayout pour afficher les �v�nements
		gestionnaireCartes = new CardLayout (5,5);
		panelAffichageEvts.setLayout(gestionnaireCartes);
		
		
		for (TreeSet<Evenement> ensEvts : frise.getEvenements()) {
			for (Evenement evt : ensEvts) {
				PanelEvenement panelEvt = new PanelEvenement(evt) ;
				//setPanelsEvts.add(panelEvt) ;
				setEvts.add(evt) ;
				panelAffichageEvts.add(panelEvt, evt.getTitre()) ;
			}// On parcourt tous les �v�nements de la frise et on cr�e un panel pour afficher cet evt
		}
		
		/*if (frise.getNbEvts() != 0) {
			gestionnaireCartes.show(panelAffichageEvts, setEvts.first().toString());
		}*/
		
		panelTableEvts = new PanelTable(frise) ;
		
		
		panelDescription.add(panelAffichageEvts, BorderLayout.CENTER);
		add(panelDescription, BorderLayout.NORTH) ;
		add(panelTableEvts, BorderLayout.SOUTH) ;
	}
	
	public void afficheEvenement(Evenement parEvt) {
		gestionnaireCartes.show(panelAffichageEvts, parEvt.getTitre());
	}


	public void setFrise(Frise parFrise) {
		frise = parFrise ;
		setEvts = new TreeSet<Evenement> (frise.getEvenements2()) ;
		
		//setPanelsEvts = new TreeSet<PanelEvenement>() ;
		
		for (Evenement evt : setEvts) {
			PanelEvenement panelEvt = new PanelEvenement (evt) ;
			//setPanelsEvts.add(panelEvt) ;
			panelAffichageEvts.add(panelEvt, evt.getTitre()) ;
		}
		
		panelTableEvts.setFrise(frise) ;
	}


	public void ajoutEvenement(Evenement evenement) {
		frise.ajout(evenement);
		panelTableEvts.ajoutEvenement(evenement) ;
		setEvts.add(evenement) ;
		PanelEvenement panelEvt = new PanelEvenement(evenement) ;
		//setPanelsEvts.add(panelEvt) ;
		panelAffichageEvts.add(panelEvt, evenement.getTitre()) ;
		//this.setFrise(parFrise);
		
	}


}
