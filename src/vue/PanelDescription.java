package vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controleur.Controleur;
import modele.Donnees;
import modele.Evenement;
import modele.Frise;

@SuppressWarnings("serial")
public class PanelDescription extends JPanel implements ActionListener {
	JLabel labelTitreFrise = new JLabel("", JLabel.CENTER);
	Frise frise ;
	public CardLayout gestionnaireCartes ;
	JPanel panelAffichageEvts;
	JButton [] tabBoutons ;
	Evenement[] tabEvts ;
	int numEvt; //num�ro de l'�v�nement affich�
	
	public PanelDescription(Frise parFrise) {
		panelAffichageEvts = new JPanel ();
		numEvt = 0;
		
		
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("<") && tabEvts.length>1) {
			gestionnaireCartes.previous(panelAffichageEvts);
			numEvt -- ;
		}//Si on clique sur le bouton pr�c�dent

		else if (evt.getActionCommand().equals(">") && tabEvts.length>1) {
			gestionnaireCartes.next(panelAffichageEvts);
			numEvt ++ ;
		}//Si on clique sur le bouton suivant
	}//gestion des interactions

	
	public Evenement getEvenementAffiche () {
		return tabEvts [numEvt] ;
	}

	public void setEvenementAffiche (Evenement evt) {
		numEvt = 0 ;
		for (Evenement evtAafficher : tabEvts) {
			if (evt.compareTo(evtAafficher) == 0) {
				 gestionnaireCartes.show(panelAffichageEvts, tabEvts[numEvt].toString());
			}
			numEvt ++ ;
		}
		
	}

	public void setFrise(Frise parFrise) {
		frise = parFrise ;
	}
	
	public void ajoutEvenement(Evenement evt) {
		this.add(new PanelEvenement(evt), evt.toString());
	}
}
