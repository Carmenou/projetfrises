package vue;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import controleur.Controleur;
import modele.Donnees;
import modele.Frise;

@SuppressWarnings("serial")
public class PanelFrise extends JPanel implements ActionListener {
	CardLayout gestCard;
	PanelCreationFrise panelCreationFrise;
	public PanelDescription panelDescription;
	PanelAjoutEvenement panelAjoutEvenement;
	Frise frise ;
	public PanelAffichage panelAffichage;
	File fichierFrise;
	PanelChargementFrise panelChargement;
	Controleur controleur;
	
	// Création du popupMenu
	JPopupMenu popupMenu;

	public PanelFrise() {
		gestCard = new CardLayout(4,4) ;
		
		this.setLayout(gestCard);
		
		//Lecture dans un fichier
		fichierFrise = new File("frises"+File.separator+"frise.ser") ;
		
		
		//Ajout des différents Panels
		
		panelCreationFrise = new PanelCreationFrise();
		this.add(panelCreationFrise, "Creation");
		
		panelChargement = new PanelChargementFrise();
		this.add(panelChargement, "Chargement");
		
		// controleur = new Controleur (frise, panelAffichage, panelCreationFrise, panelAjoutEvenement, panelChargement) ;
		if (fichierFrise.length() == 0) {
			controleur = new Controleur (this, panelCreationFrise, panelChargement) ;
		}
		
		
		if (fichierFrise.length() != 0) {
			frise = (Frise) LectureEcriture.lecture(fichierFrise) ;
			
			panelAjoutEvenement = new PanelAjoutEvenement(frise);
			this.add(panelAjoutEvenement, "Ajout");
			
			panelAffichage = new PanelAffichage(frise);
			this.add(panelAffichage, "Affichage");
			
			
			panelAffichage.setFrise(frise) ;
			
			controleur = new Controleur (this, frise, panelAffichage, panelCreationFrise, panelAjoutEvenement, panelChargement) ;
		}
		
		
		/*else {
			panelAjoutEvenement = new PanelAjoutEvenement(frise);
			this.add(panelAjoutEvenement, "Ajout");
			
			panelAffichage = new PanelAffichage(frise);
			this.add(panelAffichage, "Affichage");
			
			panelAffichage.setFrise(frise) ;
			controleur = new Controleur (frise, panelAffichage, panelCreationFrise, panelAjoutEvenement, panelChargement) ;
		}*/
		
		// Sous-menu
		popupMenu = new JPopupMenu();
		
		// Ajout des items du sous-menu
		for (String intitule : Donnees.SUBMENU_ITEMS) {
			JMenuItem item = new JMenuItem(intitule);
			item.setActionCommand(intitule);
			item.addActionListener(this);
			popupMenu.add(item);
			popupMenu.setLocation(20000, 20000);
		}
		

	} // constructeur PanelFrise()
	
	
	/**
	 * 
	 */
	public void amorcage(Frise parFrise) {
		frise = parFrise;
		panelAjoutEvenement = new PanelAjoutEvenement(frise);
		this.add(panelAjoutEvenement, "Ajout");
		
		panelAffichage = new PanelAffichage(frise);
		this.add(panelAffichage, "Affichage");
		
		
		panelAffichage.setFrise(frise) ;
		
		controleur = new Controleur (this, frise, panelAffichage, panelCreationFrise, panelAjoutEvenement, panelChargement) ;
	}
	
	
	/**
	 * 
	 */
	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand().equals("Quitter")) {
			System.exit(0);
		}
		
		if (evt.getActionCommand().equals("Help")) {
			JOptionPane.showMessageDialog(null, "Gestionnaire de Frises\n\nVer 1.0\n\nCarmen Prévot, Thomas Carpentier\n(C) All Rights Reserved", "À propos", 
					JOptionPane.INFORMATION_MESSAGE);
		}
		
		// Affichage du JPopupMenu
		else if (evt.getActionCommand().equals("Création")) {
			popupMenu.show(this, 0, 0);
		}
		
		// Affichage du PanelCreationFrise
		else if (evt.getActionCommand().equals("Créer une frise")) {
			gestCard.show(this, "Creation");
		}
		
		
		// Affichage du PanelChargementFrise
		else if (evt.getActionCommand().equals("Charger une frise")) {
			gestCard.show(this, "Chargement");
		}
		
		
		// Sinon, affichage du PanelAjoutEvenement
		else if (evt.getActionCommand().equals("Ajouter un évènement")) {

			// Affichage d'un message d'avrtissement si aucune frise n'existe
		
			if (fichierFrise.length() == 0) {
				JOptionPane.showMessageDialog(null, "Vous devez d'abord créer une frise "
						+ "avant de pouvoir y insérer des évènements", "Action impossible", 
						JOptionPane.ERROR_MESSAGE);
			}
			
			// Sinon, affichage du PanelAjoutEvenement
			else {
				gestCard.show(this, "Ajout");		
			}
		}
		
		else if (evt.getActionCommand().equals("Affichage")) {

			// Affichage d'un message d'avrtissement si aucune frise n'existe
		
			if (fichierFrise.length() == 0) {
				JOptionPane.showMessageDialog(null, "Vous devez d'abord créer une frise "
						+ "avant de pouvoir la visualiser", "Action impossible", 
						JOptionPane.ERROR_MESSAGE);
			}
			
			// Sinon, affichage du PanelAjoutEvenement
			else {
				gestCard.show(this, "Affichage");		
			}
		}
		
		else {
			gestCard.show(this, evt.getActionCommand());
		}
		
		// Affichage du PanelAffichage
		// Affichage d'un message d'avrtissement si aucune frise n'existe
		
	}

} // Classe PanelFrise
