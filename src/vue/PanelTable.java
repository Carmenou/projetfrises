package vue;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Scrollbar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.Scrollable;

import controleur.Controleur;
import modele.Donnees;
import modele.Evenement;
import modele.Frise;

public class PanelTable extends JPanel {
	
	private ModeleTable modele ;
	
	private JTable tableFrise ;
	
	private JScrollPane scrollPane ;
	
	private Frise frise ;
	
	private Evenement evtSelectionne;
	
	public JButton boutonPassage;
	
	public PanelTable (Frise parFrise) {
		
		frise = parFrise ;
		
		modele = new ModeleTable(frise) ;
		
		boutonPassage = new JButton("passage");
		boutonPassage.setActionCommand("passage");
		
		tableFrise = new JTable() ;
		tableFrise.setModel(modele);
		tableFrise.getTableHeader().setReorderingAllowed(false);
		tableFrise.getTableHeader().setResizingAllowed(false);
		tableFrise.setRowHeight(60);
		
		//On fixe la taille des colonnes
		int nbColonnes = tableFrise.getColumnCount() ;
		for (int i=0; i<nbColonnes; i++)
			tableFrise.getColumnModel().getColumn(i).setMinWidth(60);
		
		
		//On s'occupe du Renderer
		tableFrise.setDefaultRenderer(Evenement.class, new CelluleRenderer());
		
		//On le met � l'�coute des actions de la souris
		tableFrise.addMouseListener(new MouseAdapter() {

			//Surcharge de la m�thode mouseClicked
			public void mouseClicked(MouseEvent evt) {
				JTable table = (JTable) evt.getSource() ;
				ModeleTable modele = (ModeleTable) table.getModel();
				Point point = evt.getPoint() ;
				int rowIndex = table.rowAtPoint(point) ;
				int colIndex = table.columnAtPoint(point) ;
				
				//On affiche l'�v�nement en entier :
				if (modele.getValueAt(rowIndex, colIndex) != null) {
					setEvtSelectionne((Evenement) modele.getValueAt(rowIndex, colIndex));
					System.out.println("Case contenant un �v�nement" + rowIndex + colIndex);
					boutonPassage.doClick();
				}
			}//m�thode surcharg�e 
			}) ;
		
		tableFrise.getTableHeader().setFont(Donnees.font1);
		tableFrise.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	
		
		//les ascenceurs sur la table
		scrollPane = new JScrollPane(tableFrise, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED) ;
		scrollPane.setPreferredSize(new Dimension(700,180));
		this.add(scrollPane) ;
	}

	public Evenement getEvtSelectionne() {
		return evtSelectionne;
	}

	public void setEvtSelectionne(Evenement parEvtSelectionne) {
		this.evtSelectionne = parEvtSelectionne;
		this.setAlignmentY(evtSelectionne.getDate().getAnnee() - frise.getDateDebut().getAnnee());
	}
	

	public void setFrise(Frise parFrise) {
		frise = parFrise ;
	}

	public void ajoutEvenement(Evenement evenement) {
		frise.ajout(evenement);
		modele.ajoutEvenement(evenement) ;
				
	}
	
	public void enregistreEcouteur(Controleur controleur) {
		boutonPassage.addActionListener(controleur);
	}
}
