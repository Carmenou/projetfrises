package vue;

import java.awt.Color;
import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import modele.Donnees;
import modele.Evenement;

@SuppressWarnings("serial")
public class CelluleRenderer extends JLabel implements TableCellRenderer {
	
	/**
	 * Classe permettant de mettre en place un renderer pour la frise, et afficher les images des �v��nements, bien que cette derni�re soit unique
	 * @author Thomas Carpentier et Carmen Pr�vot
	 */
	public CelluleRenderer() {
		super() ;
		setOpaque(true) ;
		setHorizontalAlignment(JLabel.CENTER);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object evt,
			boolean estSelectionne, boolean aLeFocus, int lig, int col) {
		// On affiche les �v�nements de la colonne dans une info bulle
		this.setFont(Donnees.font4);

		if (evt == null) {
			setText("") ;
			setIcon(null) ;
		}
		else {
			Evenement evenement = (Evenement) evt ;
			Icon image = new ImageIcon(evenement.getAdresseImage()) ;
			this.setIcon(image) ;
		}
		
		return this;
	}

}
